
--------------------------------------------------------------------------------
Código: 1 - Descrição: No início da execução (Importação)
--------------------------------------------------------------------------------

@Definir Alfa aMsgErr;@

/* Definir Data   EDatMov;

@ Consiste Empresa e Filial @
Se(ECodEmp = 0)
  Inicio
   GeraLog("Informe código da Empresa.");
  Fim;
  
Se(ECodFil = 0)
  Inicio
   GeraLog("Informe código da Filial.");
  Fim;  

Se(EDatMov = 0)
  Inicio
   GeraLog("Informe Data de Movimento.");
  Fim;     */
Definir Numero nCodEmp;
Definir Numero nCodFil;
Definir Alfa aCodEmp;
Definir Alfa aCodFil;
Definir Alfa aCodUsu;

nCodEmp = EmpAtu; 
nCodFil = 1;
IntParaAlfa(nCodEmp, aCodEmp);
IntParaAlfa(nCodFil, aCodFil);
/*
Se((EmpAtu<>639) e  (EmpAtu<>409) e (EmpAtu<>638) e (EmpAtu<>370) e (EmpAtu<>371) e (EmpAtu<>618))
  Inicio
   aMsgErr = "Importador não liberado para a empresa corrente."; 
   GeraLog(aMsgErr);
  Fim;
              */

--------------------------------------------------------------------------------
Código: 2 - Descrição: No término da execução (Importação)
--------------------------------------------------------------------------------

Definir alfa aCorpo;
nCodUsu = CodUsu;
/*
NomeCompletoUsuario(nCodUsu,aCorpo);
CopiarAlfa(aCorpo,1,66);
EnviarEmail("sac@stc.cnt.br", "Importador - 155 (Movimentação de Estoque - VEGA/ELO)", "luciano.serra@stc.cnt.br", "","", aCorpo, "","");
*/

--------------------------------------------------------------------------------
Código: 3 - Descrição: Definição da operação (Importação)
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
Código: 4 - Descrição: Geração de chaves (Importação)
--------------------------------------------------------------------------------
Definir interno.com.senior.g5.co.ger.sid.executar sAcaoSID;
Definir Alfa   aRetSid;
Definir Alfa   aSql;
Definir Alfa   aMsgErr;
Definir Alfa   aVarAux; 
Definir Alfa   vCodPro; 
Definir Alfa   aCodDer;
Definir Alfa   vCodDep;
Definir Alfa   vCodTns;
Definir Alfa   aCodCcu;
Definir Alfa   vDatAux;
Definir Alfa   aDatAux1;
Definir Alfa   aDatAux2;
Definir Numero vVlrMov;
Definir Numero vQtdMov;
Definir Numero vCtaRed;
Definir Alfa   vDatMov;
Definir Alfa   vEstEos;
Definir Alfa   vNumDoc;
Definir Data   dDatMov;
@ Consiste Produto @
Se (vCodPro = "")
  GeraLog("Código do Produto é Obrigatório, Favor verificar.");

@ Consiste Valor do Movimento @
Se (vVlrMov <= 0)
  GeraLog("Valor do Movimento deve ser maior que '0' (zero), Favor verificar.");
  
@ Consiste Qtde do Movimento @
Se (vQtdMov <= 0)
  GeraLog("Quantidade de Movimento deve ser maior que '0' (zero), Favor verificar.");
  
@ Consiste data do Movimento @
Se (vDatMov = "")
  GeraLog("Data do Movimento invalida, Favor verificar.");

@ Consiste Transacao @
Se (vCodTns = "")
  vCodTns = "90201";   @ENTRADA POR PRODUÇÃO@     

@aEstEos = "S";  @

Se (vCodDep = "")
  vCodDep = "01";

Se (vCtaFin > 0)
  nCtaFin=vCtafin;
Senao  
  nCtaFin = 1020;

Se (vCtaRed > 0)
  nCtaRed = vCtaRed;
Senao   
  nCtaRed = 2201;

Se (vCodCcu > 0)
  nCodCcu = vCodCcu; 
Senao
  vCodCcu = 20001;

IntParaAlfa(nCodCcu, aCodCcu);
LimpaEspacos(vCodPro);
LimpaEspacos(aCodCcu);
LimpaEspacos(aCodUsu);
LimpaEspacos(aCodFil);
  
/*
AlfaParaInt(vDatMov,nVarAux);
ConverteMascara(3,nVarAux,vDatAux,"DD/MM/YYYY");
LimpaEspacos(aVarAux);   
AlfaParaData(aVarAux,dDatMov2);    
*/
/*
AlfaParaINt(vDatMov,nVarAux);
ConverteMascara(3,nVarAux,vDatMov,"DD/MM/YYYY");
LimpaEspacos(aVarAux);   
*/

aDatAux1=vDatMov;
CopiarAlfa(aDatAux1,1,2);                            
aDatAux2=aDatAux1+"/";
aDatAux1=vDatMov;
CopiarAlfa(aDatAux1,3,2);
aDatAux2=aDatAux2+aDatAux1+"/";
aDatAux1=vDatMov;
CopiarAlfa(aDatAux1,5,4);
aDatAux2=aDatAux2+aDatAux1;

AlfaParaData(aDatAux2,dDatMov);
  
@ Consiste Indicativo Entrada ou Saida @
Se ((vEstEos<>"S") e (vEstEos<>"E"))
  GeraLog("Indicativo de entrada invalido. Informe E ou S. Favor verificar.");

Sql_Criar(aSql);
Sql_DefinirComando(aSql, " SELECT E075PRO.CodPro,  \ 
                                  E075DER.CodDer   \
                             FROM E075PRO, E075DER \
                            WHERE E075PRO.CODEMP = E075DER.CODEMP \ 
                              AND E075PRO.CODPRO = E075DER.CODPRO \
                              AND E075PRO.CODEMP = :nCodEmp \
                              AND E075PRO.CODPRO = :vCodPro ");
Sql_DefinirInteiro(aSql,"nCodEmp",nCodEmp);
Sql_DefinirAlfa(aSql,"vCodPro",vCodPro);
Sql_AbrirCursor(aSql);
Se (Sql_Eof(aSql) = 0)
Inicio
  Sql_RetornarAlfa(aSql,"CodDer",aCodDer);
Fim;
Senao
Inicio
  Sql_FecharCursor(aSql);
  Sql_Destruir(aSql); 
  aMsgErr = "Produto "+vCodPro+" não existe na Empresa: "+aCodEmp+", Favor verificar.";
  GeraLog(aMsgErr);
Fim;  
Sql_FecharCursor(aSql);
Sql_Destruir(aSql); 

Sql_Criar(aSql);
Sql_DefinirComando(aSql, " SELECT max(SeqMov) as SeqMov \
                             FROM E210MVP \
                            WHERE CODEMP = :nCodEmp \
                              AND CODPRO = :vCodPro \
                              AND CODDEP = :vCodDep \
                              AND DATMOV = :dDatMov");
Sql_DefinirInteiro(aSql,"nCodEmp",nCodEmp);
Sql_DefinirAlfa(aSql,"vCodPro",vCodPro);
Sql_DefinirAlfa(aSql,"vCodDep",vCodDep);
Sql_DefinirData(aSql,"dDatMov",dDatMov);
Sql_AbrirCursor(aSql);      
Se (Sql_Eof(aSql) = 0)
Inicio
  Sql_RetornarInteiro(aSql,"SeqMov",nSeqMov);
  nSeqMov = nSeqMov + 1;
  /*
  Se(nSeqMov = 0)
    nSeqMov = 1; 
  Senao
    nSeqMov = nSeqMov + 1; 
  */
Fim;
Senao
  nSeqMov = 1;  
Sql_FecharCursor(aSql);
Sql_Destruir(aSql); 

sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "ACAO=SID.Est.Movimentar";    
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "CodEmp="+aCodEmp; 
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "CodPro="+vCodPro;
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "CodDer="+aCodDer;
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "CodTns="+vCodTns;          
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "CodDep="+vCodDep;
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "DatMov="+aDatAux2;

IntParaAlfa(vQtdMov, aVarAux);
LimpaEspacos(aVarAux);
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "QtdMov="+aVarAux;

ConverteMascara(2, vVlrMov, aVarAux, "ZZZ.ZZZ.ZZZ.ZZ9,99");       
LimpaEspacos(aVarAux);
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "VlrMov="+aVarAux;
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "FilDep="+aCodFil; 
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "EstEos="+vEstEos;  
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "UsuRes="+aCodUsu; 
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "UsuRec="+aCodUsu; 
   
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "CodCcu="+aCodCcu;

IntParaAlfa(nCtaRed, aVarAux);
LimpaEspacos(aVarAux);
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "CtaRed="+aVarAux;

IntParaAlfa(nCtaFin, aVarAux);
LimpaEspacos(aVarAux);
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "CtaFin="+aVarAux;

sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "ObsMvp=Inserido via Importação.";
sAcaoSID.SID.CriarLinha(); sAcaoSID.SID.Param = "NumDoc="+vNumDoc;
sAcaoSID.ModoExecucao = 1; @ 1 para execução local @ 
sAcaoSID.Executar();
aRetSid = sAcaoSID.Resultado;

aVarAux = aRetSid;
CopiarAlfa(aVarAux, 1, 4);
Se (aVarAux = "ERRO")
   GeraLog(aRetSid);
Senao
Inicio   
  LimpaEspacos(aRetSid);
  AlfaParaInt(aRetSid, nRet);
  Se (nRet <= 0)
    GeraLog(aRetSid);
Fim; 

--------------------------------------------------------------------------------
Código: 5 - Descrição: A cada leitura (Importação)
--------------------------------------------------------------------------------

cancel(1);
